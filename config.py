import os

TG_TOKEN = os.environ.get("TG_BOT_TOKEN")
PROXY = None
CHAT_ID = int(
    os.environ.get("CHAT_ID", "-581025811")
)  # The chat group or individual chat id of forwarded message destination
BOT_TIMEOUT = 10
BOT_RETRIES = 5
WELCOME_MESSAGE = os.environ.get("WELCOME_MESSAGE", "Hello to demo feedback bot")
HELP_MESSAGE = os.environ.get("HELP_MESSAGE", "DEMO Author: William Lee")

DB_URI = "host={} dbname={} user={} password={}".format(
    os.environ.get("DB_HOST"),
    os.environ.get("DB_NAME"),
    os.environ.get("DB_USER"),
    os.environ.get("DB_PASS"),
)
