from typing import List, Any
from config import DB_URI
import psycopg2
import psycopg2.extras

psycopg2.extras.register_uuid()
psycopg2.extras.register_json(oid=3802, array_oid=3807, globally=True)

def connect():
    return psycopg2.connect(DB_URI)

def execute(sql: str, params: List[Any] = None):
    with connect() as conn:
        with conn.cursor() as cur:
            return cur.execute(sql, params)

def fetch_one(sql: str, params: List[Any] = None):
    with connect() as conn:
        with conn.cursor() as cur:
            cur.execute(sql, params)
            return cur.fetchone()
