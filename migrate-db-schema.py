import db

def migrate(sql: str, formats = None):
    with db.connect() as conn:
        with conn.cursor() as cur:
            cur.execute(sql, formats)

migrate("""CREATE TABLE IF NOT EXISTS events (
    id UUID NOT NULL,
    type VARCHAR(255),
    data JSONB,
    collected_at timestamp with time zone,
    PRIMARY KEY (id)
)""")
