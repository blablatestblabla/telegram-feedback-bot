from typing import Any
from loguru import logger
import db
import uuid
import json
import datetime


def add_event(type: str, data: Any):
    id = uuid.uuid4()
    db.execute(
        "INSERT INTO events VALUES (%s, %s, %s, %s)",
        [id, type, json.dumps(data), datetime.datetime.now()],
    )


def message_received(received_message):
    logger.info("Received message = {}", received_message)
    add_event(
        "message_received",
        {
            "from": received_message["from"],
            "message_id": received_message["message_id"],
        },
    )


def message_forwarded(received_message, forwarded_message):
    logger.info("Forwarded message = {}", forwarded_message)
    forwarded_message = {
        "received_message_chat_id": received_message.chat.id,
        "received_message_id": received_message.message_id,
        "forwarded_message_id": forwarded_message.message_id,
    }
    add_event("message_forwarded", forwarded_message)


def find_feedback(forwarded_message_id):
    res = db.fetch_one(
        "SELECT data FROM events WHERE type = 'message_forwarded' AND data->>'forwarded_message_id' = %s",
        [forwarded_message_id],
    )
    return res[0]
