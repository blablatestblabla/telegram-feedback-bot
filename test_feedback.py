import db
import feedback
from unittest.mock import Mock

def test_forwarding_message():
    db.execute("TRUNCATE events")
    chat_attrs = {"id": "chat-123"}
    received_attrs = {"chat": Mock(**chat_attrs), "message_id": "received-1"}
    received_message = Mock(**received_attrs)
    forwarded_attrs = {"message_id": "sent-1"}
    forwarded_message = Mock(**forwarded_attrs)
    feedback.message_forwarded(received_message, forwarded_message)
    feedback_message = feedback.find_feedback("sent-1")
    assert feedback_message["received_message_id"] == "received-1"
    assert feedback_message["received_message_chat_id"] == "chat-123"
